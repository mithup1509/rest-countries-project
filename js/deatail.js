function fetchData() {
  let countryData = JSON.parse(sessionStorage.getItem("countriesData"));
  let countryName = sessionStorage.getItem("countryName");
  countryData = countryData.filter(country => country.Name.toLowerCase() === countryName.toLowerCase())[0];

  
    if (countryData.Name.toLowerCase() === countryName.toLowerCase()) {
      let mainContainer = document.createElement("div");
      mainContainer.innerHTML = `  <div class="country-image">
            <img src="${countryData["flag-image"]}" alt="moon">
            </div>
            <div class="country-details">
                <div class="country-desc">
                    <div class="main-detail">
                        <p class="country-name">${countryData.Name}</p>
                        <div class="country-regions">
                            <div class="native-name">
                                <p class="native-text">Native Name:</p>
                                <p class="native">${countryData["Native Name"]}</p>
                            </div>
                            <div class="population">
                                <p class="country-population">Population:</p>
                                <p class="total-population">${countryData.Population}</p>
                            </div>
                            <div class="region">
                                <p class="country-region">Region :</p>
                                <p class="region-name">${countryData.Region}</p>
                            </div>
                            <div class="sub-region">
                                <p class="country-subregion">SubRegion :</p>
                                <p class="subregion-name">${countryData["Sub Region"]}</p>
                            </div>
                            <div class="capital">
                                <p class="country-capital">Capital : </p>
                                <p class="caital-name">${countryData.Capital}</p>
                            </div>

                        </div>
                    </div>
                    <div class="other-detail">
                        <div class="tld">
                            <p class="tld-text">Top Level Domain:</p>
                            <p class="tld-data">${countryData["Top Level Domain"]}</p>
                        </div>
                        <div class="currency">
                            <p class="currencies">Currencies:</p>
                            <p class="currency-data">${countryData.Currencies}</p>    
                        </div>
                        <div class="Languages">
                            <p class="languages-text">Languages:</p>
                            <p class="languages-data">${countryData.Languages}</p>
                        </div>
                    </div>
                </div>
                <div class="country-borders">
                    <p class="border-text">Border countries</p>
                    <div class="border-countries">
                   
                    
                </div>
                </div>
            </div>`;
      mainContainer.className = "main-container";
      document.querySelector(".main-container").innerHTML = "";

      document.querySelector(".main-container").appendChild(mainContainer);

      let borders = document.querySelector(".border-countries");

      Array.from(countryData["Border Countries"].split(",")).forEach((data) => {
        let button = document.createElement("button");
        button.textContent = data;
        button.className = "country-button";
        button.id="country-but";
        button.onclick = buttonclick;

        borders.append(button);
      });
    }
}

function buttonclick(e) {
    if(e.target.textContent !=="No Data Available"){
  sessionStorage.setItem("countryName", e.target.textContent);
  fetchData();
    }
}
fetchData();

let darkmode=document.getElementById('darkmodeon');

darkmode.addEventListener('click',changeBackground);

function changeBackground(e){
    if(document.querySelector("#theme-tag").textContent==="Dark Mode"){
        document.querySelector("#theme-tag").textContent="Light Mode";
        document.getElementById('head').style.backgroundColor="hsl(209, 23%, 22%)";
        document.getElementById('heading').style.color="hsl(0, 0%, 100%)";
        document.getElementById('mainbody').style.backgroundColor=" hsl(207, 26%, 17%)"; 
        document.getElementById('fullcontainer').style.color="hsl(0, 0%, 100%)";
        document.getElementById('back-button').style.backgroundColor="hsl(209, 23%, 22%)";
        document.getElementById('country-but').style.backgroundColor="hsl(209, 23%, 22%)";
        document.getElementById('country-but').style.color="hsl(0, 0%, 100%)";


    }else{
        document.querySelector("#theme-tag").textContent="Dark Mode";
        document.getElementById('head').style.backgroundColor="hsl(0, 0%, 100%)";
        document.getElementById('heading').style.color="hsl(200, 15%, 8%)";
        document.getElementById('mainbody').style.backgroundColor=" hsl(0, 0%, 98%)"; 
        document.getElementById('fullcontainer').style.color="hsl(200, 15%, 8%)";
        document.getElementById('back-button').style.backgroundColor="hsl(0, 0%, 100%)";
        document.getElementById('country-but').style.backgroundColor="hsl(0, 0%, 100%)";
        document.getElementById('country-but').style.color="hsl(200, 15%, 8%)";
    }
}