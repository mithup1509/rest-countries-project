
function fetchdata() {
  return fetch("https://restcountries.com/v3.1/all");
}

fetchdata()
  .then((data) => {
    return data.json();
  })
  .then((data) => {
    let eachCountryData = [];
    let countryInitials = {};
    data.forEach((element) => {
      countryInitials[element.cca3] = element.name.common;
    });

    // console.log(countryInitials);

    data.forEach((element) => {
      let eachdata = {};
      if (element.flag) {
        eachdata["flag-image"] = element.flags.svg;
      } else {
        eachdata["flag-image"] = "";
      }

      if (element.name.common) {
        eachdata["Name"] = element.name.common;
      } else {
        eachdata["Name"] = "No Data available";
      }

      if (element.name.nativeName) {
        eachdata["Native Name"] = Object.entries(
          element.name.nativeName
        )[0][1].common;
      } else {
        eachdata["Native Name"] = "No Data available";
      }
      if (element.population) {
        eachdata["Population"] = element.population;
      } else {
        eachdata["Population"] = "No Data available";
      }
      if (element.region) {
        eachdata["Region"] = element.region;
      } else {
        eachdata["Region"] = "No Data available";
      }
      if (element.subregion) {
        eachdata["Sub Region"] = element.subregion;
      } else {
        eachdata["Sub Region"] = "No Data available";
      }
      if (element.capital) {
        eachdata["Capital"] = element.capital[0];
      } else {
        eachdata["Capital"] = "No Data available";
      }
      if (element.tld) {
        eachdata["Top Level Domain"] = element.tld.join(",").replace(".", "");
      } else {
        eachdata["Top Level Domain"] = "No Data available";
      }
      if (element.currencies) {
        let curr = {};
        Object.entries(element.currencies).forEach((each) => {
          curr[each[0]] = each[0];
        });
        eachdata["Currencies"] = Object.values(curr).join(",");
      } else {
        eachdata["Currencies"] = "No Data available";
      }
      if (element.languages) {
        eachdata["Languages"] = Object.values(element.languages).join(",");
      } else {
        eachdata["Languages"] = "No Data available";
      }
      let countries = [];
      if (element.borders) {
        element.borders.forEach((eachborder) => {
          if (countryInitials[eachborder]) {
            countries.push(countryInitials[eachborder]);
          }
        });
        eachdata["Border Countries"] = countries.join(",");
      } else {
        eachdata["Border Countries"] = "No Data Available";
      }
      // console.log(eachdata);
      eachCountryData.push(eachdata);
    });

    return eachCountryData;
  })
  .then((data) => {
    sessionStorage.setItem("countriesData", JSON.stringify(data));
    createCards();
  })
  .catch((err) =>{
     console.error(err)
    });


function createCards() {
  let CountryData = JSON.parse(sessionStorage.getItem("countriesData"));

console.log(CountryData);
  CountryData.forEach((data) => {
    

    const countryDetail = document.createElement("div");

    countryDetail.innerHTML = `<div class="country-image">
          <img src="${data["flag-image"]}" alt="image">
      </div>
    <div class="country-desc">
          <p class="country-name">${data.Name}</p>
      <div class="country-deatails">
        <div class="population">
          <p class="country-population">Population:</p>
          <p class="total-population">${data.Population}</p>
        </div>
        <div class="region">
          <p class="country-region">Region :</p>
          <p class="region-name">${data.Region}</p>
        </div>
        <div class="capital">
          <p class="country-capital">Capital : </p>
          <p class="caital-name">${data.Capital}</p>
        </div>
      </div>
    </div>`;

    countryDetail.setAttribute("class","country")
    document
      .querySelector(".country-data")
      .appendChild(countryDetail);
  });

  let filter = document.getElementById("filter");
  filter.addEventListener("click", filterData);

  function filterData(e) {
    e.preventDefault();
    let countryTag = document.getElementsByClassName("region-name");
 
    Array.from(countryTag).forEach((element) => {
      let eachCountry = element.parentNode.parentNode.parentNode.parentNode;
      let searchValue=document.getElementById('search-input').value;
      let countryNames= eachCountry.getElementsByClassName('country-name');
      
      Array.from(countryNames).forEach((data)=>{
        if (
          (data.textContent.toLowerCase().indexOf(searchValue.toLowerCase())!=-1) && ((element.textContent
            .toLowerCase()
            .indexOf(e.target.value.toLowerCase()) != -1) || (e.target.value.localeCompare("Filter by Region")===0 || e.target.value.localeCompare("all")===0) ) ){
          eachCountry.style.display = "block";
        } else {
          eachCountry.style.display = "none";
        }
      
      })

    
    });
  }

  let searchcard=document.getElementById('searchcard');
  searchcard.addEventListener('keyup',searchingCard);


  function searchingCard(e){

  let searchValue=document.getElementById('search-input').value;

let countryNames=document.getElementsByClassName('country-name');

Array.from(countryNames).forEach((element)=>{
let eachOneCard=element.parentNode.parentNode;
console.log(eachOneCard);
let countryTag =eachOneCard.getElementsByClassName("region-name");


Array.from(countryTag).forEach((data) => {
  let filter = document.getElementById("filter").value;
  if((element.textContent.toLowerCase().indexOf(searchValue.toLowerCase())!=-1) && (data.textContent.toLowerCase().indexOf(filter.toLowerCase())!=-1 || (filter.localeCompare("Filter by Region")===0 || filter.localeCompare("all")===0) )){
    
eachOneCard.style.display="block";
  }else{
    eachOneCard.style.display="none";
  }
})
 
  

  
  
})
  }

let darkmode=document.getElementById('darkmodeon');

darkmode.addEventListener('click',changeBackground);

function changeBackground(e){
 
  if(document.querySelector("#theme-tag").textContent==="Dark Mode"){
    document.querySelector("#theme-tag").textContent="Light Mode";
document.getElementById('head').style.backgroundColor="hsl(209, 23%, 22%)";
document.getElementById('maincontainer').style.backgroundColor=" hsl(207, 26%, 17%)"; 
document.getElementById('first-container').style.color="hsl(0, 0%, 100%)";
document.getElementById('filter').style.backgroundColor="hsl(209, 23%, 22%)";
document.getElementById('filter').style.color="hsl(0, 0%, 100%)";
document.getElementById('searchcard').style.backgroundColor="hsl(209, 23%, 22%)";
document.getElementById('searchcard').style.color="hsl(0, 0%, 100%)";

document.getElementById('search-input').style.backgroundColor="hsl(209, 23%, 22%)";
document.getElementById('search-input').style.color="hsl(0, 0%, 100%)";
document.getElementById('filterbutton').style.backgroundColor="hsl(209, 23%, 22%)";
document.getElementById('filterbutton').style.color="hsl(0, 0%, 100%)";

}else{
  document.querySelector("#theme-tag").textContent="Dark Mode";
  document.getElementById('head').style.backgroundColor="hsl(0, 0%, 100%)";
document.getElementById('maincontainer').style.backgroundColor=" hsl(0, 0%, 98%)"; 
document.getElementById('first-container').style.color="hsl(200, 15%, 8%)";
document.getElementById('filter').style.backgroundColor="hsl(0, 0%, 100%)";
document.getElementById('filter').style.color="hsl(200, 15%, 8%)";
document.getElementById('searchcard').style.backgroundColor="hsl(0, 0%, 100%)";
document.getElementById('searchcard').style.color="hsl(200, 15%, 8%)";

document.getElementById('search-input').style.backgroundColor="hsl(0, 0%, 100%)";
document.getElementById('search-input').style.color="hsl(200, 15%, 8%)";
document.getElementById('filterbutton').style.backgroundColor="hsl(0, 0%, 100%)";
document.getElementById('filterbutton').style.color="hsl(200, 15%, 8%)";
}
}


document.querySelector(".country-data").addEventListener('click',(event)=>{
  let div = (event.target.closest(".country"));
  let countryName = div.querySelector(".country-name").textContent;
  sessionStorage.setItem("countryName", countryName);
  window.location.href = "./html/detail.html";
  });




}
